import React from 'react';
import axios from 'axios';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';
import Products from './components/Products';
import Loader from './components/Loader';
import CreateProduct from './components/CreateProduct';
import Header from './components/Header';
import UpdateProduct from './components/UpdateProduct';
import { Switch } from 'react-router-dom/cjs/react-router-dom.min';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    }

    this.state = {
      products: [],
      status: this.API_STATES.LOADING,
      errorMessage: "",
    }

    this.URL = 'https://fakestoreapi.com/products/';
  }

  fetchData = (url) => {
    this.setState({
      status: this.API_STATES.LOADING,
    }, () => {
      axios.get(url)
        .then((response) => {
          this.setState({
            status: this.API_STATES.LOADED,
            products: response.data,
          })

        })
        .catch((err) => {
          this.setState({
            status: this.API_STATES.ERROR,
            errorMessage: "An API error occured. Please try again in a few minutes.",
          })
        })
    })
  }

  componentDidMount = () => {
    this.fetchData(this.URL);
  }

  handleClickDelete = (id) => {
    let newProducts = this.state.products.filter((product) => {
      return product.id !== id;
    });
    this.setState({
      products: newProducts,
    });
  }

  getNewId = () => {
    const maxId = this.state.products.reduce((getMaxId, product) => {
      if (product.id > getMaxId) {
        getMaxId = product.id;
        return getMaxId;
      } else {
        return getMaxId;
      }
    }, 0);

    return maxId + 1;
  }

  handleClickCreate = (title, description, price, category, image) => {

    let newProduct = {};

    newProduct.title = title;
    newProduct.description = description;
    newProduct.price = price;
    newProduct.category = category;
    newProduct.image = image;
    newProduct.id = this.getNewId();
    newProduct.rating = {
      rate: 0,
      count: 0,
    }

    let newProducts = this.state.products;
    newProducts.unshift(newProduct);
    // console.log(newProducts);

    this.setState({
      products: newProducts,
    })
  }

  handleClickUpdate = (title, description, price, category, id) => {
    const updatedProducts = this.state.products;

    console.log(id)

    const updatedProduct = updatedProducts.find((product) => {
      return product.id === id;
    });

    updatedProduct.title = (title !== '') ? title : updatedProduct.title;

    updatedProduct.category = (category !== '') ? category : updatedProduct.category;
    updatedProduct.description = (description !== '') ? description : updatedProduct.description;
    updatedProduct.price = (price !== '') ? price : updatedProduct.price;

    this.setState({
      products: updatedProducts
    });

  }

  render() {

    return (
      <Router>
        <div className="App">
          <Header />
          {this.state.status === this.API_STATES.ERROR &&

            <div className='fetch-fail'>
              <h2> {this.state.errorMessage} </h2>
            </div>
          }

          {this.state.status === this.API_STATES.LOADING &&
            <Loader />
          }

          {this.state.status === this.API_STATES.LOADED && this.state.products.length === 0 &&
            <div className="no-data">
              <h2>No products available at the moment. Please try again.</h2>
            </div>
          }

          {this.state.status === this.API_STATES.LOADED && this.state.products.length > 0 &&
            <Switch >
              <Route path="/" exact>
                <div className="main-coontainer">
                  <ul>
                    <Products products={this.state.products}
                      handleClickDelete={this.handleClickDelete} />
                  </ul>
                </div>
              </Route>
              <Route path="/new-product">
                <CreateProduct handleClickCreate={this.handleClickCreate} />
              </Route>
              <Route path="/product/:id" render={(routeProps) => {
                return <UpdateProduct
                  id={routeProps.match.params.id}
                  product={this.state.products.find((product) => {
                    return String(product.id) === (routeProps.match.params.id);
                  })}
                  handleClickUpdate={this.handleClickUpdate} />
              }} />
            </Switch>
          }
        </div>
      </Router>
    );
  }
}

export default App;

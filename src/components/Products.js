import React from "react";
import { Link } from "react-router-dom";
import './Products.css';


class Products extends React.Component {

    render() {
        return (
            this.props.products.map((product) => (
                <li key={product.id} >
                    <div className="main-container-wrapper">
                        <div className="update-icon">
                            <img src={product.image} alt={product.title}></img>
                        </div>
                        <div className="content-container">
                            <h2>{product.title}</h2>
                            <h4>{product.category}</h4>
                            <h3>${product.price}</h3>
                            <div className="rating">
                                <p>Rating: {product.rating.rate}</p>
                                <span>{"(" + product.rating.count + ")"}</span>
                            </div>
                            <p>{product.description}</p>
                        </div>
                        <div className="button-section">
                            <Link to={`/product/${product.id}`}>
                                <button className="update-btn"> update <i className="fa-solid fa-pen-to-square"></i>
                                </button>
                            </Link>

                            <button className="delete-btn" onClick={() => {
                                this.props.handleClickDelete(product.id)
                            }}>
                                delete <i className="fa-solid fa-trash"></i>
                            </button>
                        </div>
                    </div>
                </li>
            ))
        )
    }
}


export default Products;